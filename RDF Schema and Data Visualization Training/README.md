# RDF Schema and Data Visualization Training

This training is based on the SPHN RDF schema [2021.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2) version. To view the newest version of the RDF schema click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).


The following resources are associated to the training:

- [GraphDB usage](https://graphdb.ontotext.com/documentation/8.4/free/usage.html)
- [SPHN ReadTheDocs - Visually explore data with GraphDB](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/data_exploration.html)
- [SPHN GitLab - Mock data](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/tree/master/mock_data)
- [SPHN GitLab - SPARQL queries]( https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/tree/master/RDF%20Schema%20and%20Data%20Visualization%20Training)


For access to the external terminologies (minified SNOMED CT and LOINC) please [e-mail the DCC](dcc@sib.swiss).
