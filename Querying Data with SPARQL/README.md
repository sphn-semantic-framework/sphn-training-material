# Querying Data with SPARQL

This training is based on the SPHN RDF schema [2021.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2) version. To view the newest version of the RDF schema click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).


The following resources are associated to the training:

- [SPHN ReadTheDocs – Querying Data with SPARQL](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/sparql.html)
- [SPARQL Documentation](https://www.w3.org/TR/sparql11-query/)
- [SPHN GitLab - Mock data](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/tree/master/mock_data)

For access to the external terminologies please [e-mail the DCC](dcc@sib.swiss).
