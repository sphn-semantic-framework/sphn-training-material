# Advanced SPARQL queries and best practices

This training consists of running advanced SPARQL queries against a set of mock RDF data.

The goal of the hands on is to explore how to write and execute queries according to specific
questions.

The mock data use for queries can be found in the [data](./data/mock-data.zip) folder.

This training (and the mock data) is based on the [SPHN RDF Schema 2024.2](https://www.biomedit.ch/rdf/sphn-schema/sphn/2024/2).

**Note:** In addition to the mock data and the SPHN RDF Schema, you would also have to load
external terminologies like SNOMED-CT, ICD-10-GM, CHOP, ATC, UCUM. For access to the external
terminologies please go through the [BioMedIT Portal - Terminology Service](https://portal.dcc.sib.swiss/).
