# SPHN Training material

This repository contains training material from the SPHN Training sessions. 

## Training videos

The training videos can be found on the SPHN Website

* SPHN Data Ecosystem for FAIR Data  https://youtu.be/pqV0qp4oisM
* FAIR principles in practice for health data https://sphn.ch/training/fair-principles-in-practice-for-health-data/
* Semantic Standards https://sphn.ch/training/semantic-standards-training/
* How to create a concept for the SPHN Dataset https://sphn.ch/training/create-a-concept-for-sphn-dataset/
* Training Primer (RDF and SPARQL)  https://sphn.ch/training/sphn-rdf-training-primer/
* Expanding the SPHN RDF Schema  https://sphn.ch/training/protege-training/
* RDF Schema and Data Visualization  https://sphn.ch/training/datavisualization/
* Querying Data with SPARQL  https://sphn.ch/training/querying-data-with-sparql/
* How to use Python and R with RDF Data https://sphn.ch/training/pythonandr/ 
* Validate Graph Data with SHACL https://sphn.ch/validate-graph-data-with-shacl/



