# Create RDF Data

This training consists of a Jupyter Notebook that illustrates how to create RDF data using Python and [RDFLib](https://rdflib.readthedocs.io/en/stable/).


The notebook can be run as follows:

```sh
pip install -r requirements.txt

jupyter notebook
```

Then open the `Create-RDF-Data.ipynb`
