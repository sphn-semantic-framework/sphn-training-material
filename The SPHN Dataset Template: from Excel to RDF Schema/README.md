# The SPHN Dataset Template: from Excel to RDF schema

This training is based on the [SPHN Dataset Template 2024.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/tree/master/templates/dataset_template).

The SPHN Schema Forge can be accessed at:  https://schemaforge.dcc.sib.swiss.

More information can be found in the [SPHN ReadTheDocs](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/project_schema_generation.html#option-1-produce-an-rdf-schema-from-the-sphn-dataset-template). 
