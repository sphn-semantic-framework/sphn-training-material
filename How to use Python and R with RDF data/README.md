# How to use Python and R with RDF Data

This training was initially based on the SPHN RDF Schema [2021.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2) version. 
Documents and Notebooks are now updated to the 2023.2 version of SPHN RDF Schema.

To view the newest version of the RDF schema click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).

The following resources are associated to the training:

-	[SPHN ReadTheDocs - Use Python and R with RDF data](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/data_analysis.html) 
- [SPHN GitLab - Mock data](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/blob/master/mock_data/mock-data-sparqls.zip)
- [SPHN GitLab - R and Python Notebook](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/tree/master/How%20to%20use%20Python%20and%20R%20with%20RDF%20data)

The archived versions of the notebooks are available [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/tree/master/How%20to%20use%20Python%20and%20R%20with%20RDF%20data/archive).



For access to the external terminologies please go through the [BioMedIT Portal - Terminology Service](https://portal.dcc.sib.swiss/).

