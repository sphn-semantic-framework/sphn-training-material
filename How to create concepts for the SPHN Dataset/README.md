# How to create a concept for the SPHN Dataset

This training is based on the SPHN Dataset [2022.1](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2022-1/dataset) version. To view the newest version of the dataset click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/master/dataset).

The following resources are associated to the training:

-	[SPHN ReadTheDocs – Guiding principles for concept design](https://sphn-semantic-framework.readthedocs.io/en/latest/sphn_framework/sphndataset.html#guiding-principles-for-concept-design)
