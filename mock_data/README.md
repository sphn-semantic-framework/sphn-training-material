# Mock data

[sphn-mock-data-2021-2.ttl](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/blob/master/mock_data/sphn-mock-data-2021-2.ttl): Turtle file of the mock data used in the following training videos:
* [RDF Schema and Data Visualization Training](https://sphn.ch/training/datavisualization/)
* [Querying Data with SPARQL](https://sphn.ch/training/querying-data-with-sparql/)


Mock data used in documentation (2023):
- [mock-data-exploration.zip](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/blob/master/mock_data/mock-data-exploration.zip): Mock data used for user guide on [data exploration with GraphDB](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/data_exploration.html#)
- [mock-data-sparqls.zip](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/blob/master/mock_data/mock-data-sparqls.zip): Mock data used for user guide with [sparql queries](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/sparql.html)
