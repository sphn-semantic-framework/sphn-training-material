# FAIRification of Terminologies

The training explores best practices in making terminologies Findable, Accessible, Interoperable, and Reusable (FAIR).

The training covers the following topics:

- Introduction to terminologies
- Best practices for making terminologies FAIR
- Semantic Web standards for FAIRification of terminologies
- Versioning strategies for better management of terminologies

The training is intended for researchers, data analysts, data engineers, and data managers interested in terminology management
and how to go about bringing their terminologies into RDF, especially in the context of SPHN.


Resources associated with this training:

- [The slides from the webinar](FAIRification_of_terminologies.pdf)
- [Script for converting a sample ATC hierarchy into RDF](scripts/atc_script.py)


Additional resources that are recommended as a follow up to this training:

- [Terminology Service]( https://terminology.dcc.sib.swiss)
- [Ontology pipelines for the DCC Terminology Service](https://git.dcc.sib.swiss/dcc/biomedit-rdf)
- [Documentation on External Terminologies in SPHN](https://sphn-semantic-framework.readthedocs.io/en/latest/external_resources/external_resources.html)
- [FAIRification of External Terminologies in RDF](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/project_terminologies.html)
- [SPHN Dataset Template](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/blob/master/templates/dataset_template/)
- [Documentation on generating a project specific schema](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/project_schema_generation.html)
