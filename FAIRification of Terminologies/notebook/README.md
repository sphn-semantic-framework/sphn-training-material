# Terminology to RDF notebook

This repository provides a hands on tutorial on how to go about transforming
an external terminology into RDF using Python.

The [External terminologies to RDF.ipynb](External terminologies to RDF.ipynb) notebook provides a step-by-step example of transforming a Pizza vocabulary into an RDF form.

The original Pizza vocabulary is provided as an excel file: [pizza_vocabulary.xlsx](pizza_vocabulary.xlsx)


## Installing dependencies

You can use pip to install dependencies required for running the examples in the notebook:

```sh
pip install -r requirements.txt
```

If you do not have Jupyter installed then you can install it via:

```sh
pip install jupyter
```

## Running the notebook

Once the dependencies are installed, you can run the notebook using Jupyter as follows:

```sh
jupyter notebook "External terminologies to RDF.ipynb"
```

This should open the notebook in your default browser.
