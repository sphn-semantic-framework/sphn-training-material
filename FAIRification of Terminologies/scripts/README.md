# Scripts

This directory contains the script that translates a sample ATC hierarchy into RDF.


## Installation

### Set up a virtual environment

First set up a Python virtual environment (recommended but optional)

```sh
python3 -m venv env
```

Then activate this virtual environment:

```sh
source env/bin/activate
```

### Install dependencies

The [atc_script.py]() requires RDFLib Python package to run.

You can install this dependency using `pip`:

```sh
pip install -r requirements.txt
```

## Run the script

You can run the script as follows:

```sh
python atc_script.py
```

This should give you the following output:

```sparql
@prefix atc: <https://www.whocc.no/atc_ddd_index/?code=> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix sphn-atc: <https://biomedit.ch/rdf/sphn-resource/atc/> .

sphn-atc:ATC a rdfs:Class ;
    rdfs:label "ATC" .

atc:A a rdfs:Class ;
    rdfs:label "ALIMENTARY TRACT AND METABOLISM" ;
    rdfs:subClassOf sphn-atc:ATC .

atc:A02 a rdfs:Class ;
    rdfs:label "DRUGS FOR ACID RELATED DISORDERS" ;
    rdfs:subClassOf atc:A .

atc:A02A a rdfs:Class ;
    rdfs:label "ANTACIDS" ;
    rdfs:subClassOf atc:A02 .

atc:A02AB a rdfs:Class ;
    rdfs:label "Aluminium compounds" ;
    rdfs:subClassOf atc:A02A .

atc:A02AB02 a rdfs:Class ;
    rdfs:label "aluminium phosphate" ;
    rdfs:subClassOf atc:A02AB .

sphn-atc: a owl:Ontology ;
    dc:rights "RDF version of ATC (https://www.whocc.no/atc_ddd_index/)  developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). The copyright follows instructions by the WHO Collaborating Centre for Drug Statistics Methodology (https://www.whocc.no/copyright_disclaimer/)." ;
    dc:title "Anatomical Therapeutic Chemical Classification (ATC)" ;
    owl:versionIRI <https://biomedit.ch/rdf/sphn-resource/atc/2022/1> .
```