from rdflib import Graph, URIRef, Literal
from rdflib import RDF, RDFS, OWL, DC


# Setting up the graph with general metadata

ATC_NAMESPACE = URIRef("https://www.whocc.no/atc_ddd_index/?code=")
ATC_SPHN_NAMESPACE = URIRef("https://biomedit.ch/rdf/sphn-resource/atc/")

graph = Graph()
graph.bind("rdfs", RDFS)
graph.bind("rdf", RDF)
graph.bind("atc", ATC_NAMESPACE)
graph.bind("owl", OWL)
graph.bind("dc", DC)


# Setting up the graph with terminology metadata

ATC_VERSION = URIRef("https://biomedit.ch/rdf/sphn-resource/atc/2022/1")
ATC_ROOT = ATC_SPHN_NAMESPACE + "ATC"

graph.bind("sphn-atc", ATC_SPHN_NAMESPACE)
graph.add((ATC_SPHN_NAMESPACE, OWL.versionIRI, ATC_VERSION))
graph.add((ATC_SPHN_NAMESPACE, RDF.type, OWL.Ontology))

TITLE = Literal("Anatomical Therapeutic Chemical Classification (ATC)")
graph.add((ATC_SPHN_NAMESPACE, DC.title, TITLE))

COPYRIGHT = Literal(
	"RDF version of ATC (https://www.whocc.no/atc_ddd_index/) " +
	" developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics)." +
	" The copyright follows instructions by the WHO Collaborating Centre for Drug Statistics Methodology (https://www.whocc.no/copyright_disclaimer/)."
)
graph.add((ATC_SPHN_NAMESPACE, DC.rights, COPYRIGHT))


# Building the graph

graph.add((ATC_ROOT, RDF.type, RDFS.Class))
graph.add((ATC_ROOT, RDFS.label, Literal("ATC")))

term1 = ATC_NAMESPACE + "A"
graph.add((term1, RDF.type, RDFS.Class))
graph.add((term1, RDFS.label, Literal("ALIMENTARY TRACT AND METABOLISM")))
graph.add((term1, RDFS.subClassOf, ATC_ROOT))

term2 = ATC_NAMESPACE + "A02"
graph.add((term2, RDF.type, RDFS.Class))
graph.add((term2, RDFS.label, Literal("DRUGS FOR ACID RELATED DISORDERS")))
graph.add((term2, RDFS.subClassOf, term1))

term3 = ATC_NAMESPACE + "A02A"
graph.add((term3, RDF.type, RDFS.Class))
graph.add((term3, RDFS.label, Literal("ANTACIDS")))
graph.add((term3, RDFS.subClassOf, term2))

term4 = ATC_NAMESPACE + "A02AB"
graph.add((term4, RDF.type, RDFS.Class))
graph.add((term4, RDFS.label, Literal("Aluminium compounds")))
graph.add((term4, RDFS.subClassOf, term3))

term5 = ATC_NAMESPACE + "A02AB02"
graph.add((term5, RDF.type, RDFS.Class))
graph.add((term5, RDFS.label, Literal("aluminium phosphate")))
graph.add((term5, RDFS.subClassOf, term4))

print(graph.serialize())