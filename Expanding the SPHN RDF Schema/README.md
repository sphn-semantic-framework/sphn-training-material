# Expanding the SPHN RDF Schema

This training is based on the SPHN RDF schema [2021.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2/ontology) version. To view the newest version of the RDF schema or the template ontology, click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).


The following resources are associated to the training:
- [SPHN ReadTheDocs - Generate a SPHN project-specific ontology](https://sphn-semantic-framework.readthedocs.io/en/latest/user_guide/ontology_generation.html)
- [SPHN GitLab - Template ontology 2021.2 version](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2/template_ontology)



For access to the external terminologies please go through the [BioMedIT Portal - Terminology Service](https://portal.dcc.sib.swiss/).

