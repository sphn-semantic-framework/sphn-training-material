# Training Primer (RDF and SPARQL)

This training is based on the SPHN RDF schema [2021.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2) version. To view the newest version of the RDF schema click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).


The following resources are associated to the training:

- [RDF Primer](https://www.w3.org/TR/rdf11-primer/)
- [RDF Concepts &amp; Abstract Syntax](https://www.w3.org/TR/rdf11-concepts/)
- [SPARQL](https://www.w3.org/TR/sparql11-query/)
