# Validate Graph Data with SHACL

This training is based on the SPHN RDF schema [2021.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/2021-2/ontology) version. To view the newest version of the RDF schema click [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology).


The following resources are associated to the training:

-	[SHACL Documentation](https://www.w3.org/TR/shacl/)
